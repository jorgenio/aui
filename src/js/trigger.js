(function ($, skate) {
    'use strict';

    function findControlled(trigger) {
        return document.getElementById(trigger.getAttribute('aria-controls'));
    }

    function hide(e) {
        var controls = findControlled(e.target);

        if (!controls.isVisible()) {
            return;
        }

        controls.hide();
    }

    function show(e) {
        var controls = findControlled(e.target);

        if (controls.isVisible()) {
            return;
        }

        controls.show();
    }

    skate('data-aui-trigger', {
        type: skate.types.ATTR,
        insert: function(trigger) {
            trigger.addEventListener('click', function(e) {
                var result;
                if (!trigger.isDisabled()) {
                    result = findControlled(trigger).isVisible() ? hide(e) : show(e);
                    e.preventDefault();
                }
                return result;
            });
        },
        prototype: {
            disable: function() {
                this.setAttribute('aria-disabled', 'true');
            },
            enable: function() {
                this.setAttribute('aria-disabled', 'false');
            },
            isDisabled: function() {
                return this.getAttribute('aria-disabled') === 'true';
            }
        }
    });

}(window.jQuery, window.skate || require('skate')));