(function($) {

    /**
     * Singleton layerManager instance
     * @type {LayerManager}
     */
    AJS.LayerManager.global = new AJS.LayerManager();

    $(document).on('keydown', function(e) {
        if (e.keyCode === AJS.keyCode.ESCAPE) {
            var $popped = AJS.LayerManager.global.popTopIfNonModal();
            if ($popped) {
                e.preventDefault();
            }
        }
    }).on('click', '.aui-blanket', function(e) {
        var $popped = AJS.LayerManager.global.popUntilTopBlanketed();
        if ($popped) {
            e.preventDefault();
        }
    });


}(AJS.$));