(function($) {
    
    var $overflowEl;

    /**
     *
     * Dims the screen using a blanket div
     * @param useShim deprecated, it is calculated by dim() now
     */
    AJS.dim = function (useShim, zIndex) {

        if  (!$overflowEl) {
            $overflowEl = $(document.body);
        }

        if (useShim === true) {
            useShimDeprecationLogger();
        }

        if (!AJS.dim.$dim) {
            AJS.dim.$dim = AJS("div").addClass("aui-blanket");
            AJS.dim.$dim.attr('tabindex', '0'); //required, or the last element's focusout event will go to the browser
            if (zIndex) {
                AJS.dim.$dim.css({zIndex: zIndex});
            }
            if ($.browser.msie) {
                AJS.dim.$dim.css({width: "200%", height: Math.max($(document).height(), $(window).height()) + "px"});
            }
            $("body").append(AJS.dim.$dim);
            
            AJS.dim.cachedOverflow = $overflowEl.css("overflow");
            $overflowEl.css("overflow", "hidden");
        }

        return AJS.dim.$dim;
    };

    /**
     * Removes semitransparent DIV
     * @see AJS.dim
     */
    AJS.undim = function() {
        if (AJS.dim.$dim) {
            AJS.dim.$dim.remove();
            AJS.dim.$dim = null;

            if (AJS.dim.$shim) {
                AJS.dim.$shim.remove();
                AJS.dim.$shim = null;
            }

            $overflowEl && $overflowEl.css("overflow",  AJS.dim.cachedOverflow);

            // Safari bug workaround
            if ($.browser.safari) {
                var top = $(window).scrollTop();
                $(window).scrollTop(10 + 5 * (top == 10)).scrollTop(top);
            }
        }
    };

    var useShimDeprecationLogger = AJS.deprecate.getMessageLogger("useShim", "nothing now has it is calculated by dim()", '[deprecation version]', undefined);

}(AJS.$));