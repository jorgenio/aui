(function ($) {
    'use strict';

    var has = Object.prototype.hasOwnProperty;
    var toString = Object.prototype.toString;

    function toSentenceCase (str) {
        str += '';
        if (!str) {
            return '';
        }
        return str.charAt(0).toUpperCase() + str.substring(1);
    }

    var deprecationCalls = [];

    /**
     * Return a function that logs a deprecation warning to the console the first time it is called.
     *
     * @param {string|Function} displayNameOrShowMessageFn the name of the thing being deprecated. Alternatively, a function to be returned
     * @param {string} [alternativeName] the name of the alternative thing to use
     * @param {string} [sinceVersion] the version this has been deprecated since
     * @param {string} [removeInVersion] the version this will be removed in
     * @param {integer} [stackDepthOffset=0] the offset of the calling functions stack frame.
     * @return {Function} that logs the warning
     */
    function getShowDeprecationMessage(displayNameOrShowMessageFn, alternativeName, sinceVersion, removeInVersion, stackDepthOffset) {
        if (typeof displayNameOrShowMessageFn === 'function') {
            return displayNameOrShowMessageFn;
        }
        var called = false;
        return function() {
            var deprecatedLocation = getDeprecatedLocation(stackDepthOffset ? stackDepthOffset : 0);
            // Only log once if the stack frame doesn't exist to avoid spamming the console/test output
            if ((!called && !deprecatedLocation) || (deprecatedLocation && deprecationCalls.indexOf(deprecatedLocation) == -1)) {
                called = true;
                var message = 'DEPRECATED - ' + toSentenceCase(displayNameOrShowMessageFn) + ' has been deprecated' + (sinceVersion ? ' since ' + sinceVersion : '') +
                    ' and will be removed in ' + (removeInVersion || 'a future release') + '.';
                if (alternativeName) {
                    message += ' Use ' + alternativeName + ' instead.';
                }

                if (deprecatedLocation === '') {
                    console.log(message + ' \n ' + 'No stack trace of the deprecated usage is available in your current browser.');
                } else {
                    console.log(message + ' \n ' + deprecatedLocation);
                    deprecationCalls.push(deprecatedLocation);
                }
            }
        };
    }

    function getDeprecatedLocation(stackDepthOffset) {
        var err = new Error();
        var stack = err.stack || err.stacktrace;
        var stackMessage = (stack && stack.replace(/^Error\n/, "")) || '';

        if (stackMessage) {
            stackMessage = stackMessage.split('\n');
            return stackMessage[stackDepthOffset + 2];
        }
        return stackMessage;
    }

    /**
     * Returns a wrapped version of the function that logs a deprecation warning when the function is used.
     * @param {Function} fn the fn to wrap
     * @param {string|Function} displayNameOrShowMessageFn the name of the fn to be displayed in the message. Alternatively, a function to log deprecation warnings
     * @param {string} alternativeName the name of an alternative function to use
     * @param {string} sinceVersion the version this has been deprecated since
     * @param {string} removeInVersion the version this will be removed in
     * @param {integer} [stackDepthOffset=0] the offset of the calling functions stack frame.
     * @return {Function} wrapping the original function
     */
    function deprecateFunctionExpression(fn, displayNameOrShowMessageFn, alternativeName, sinceVersion, removeInVersion, stackDepthOffset) {
        var showDeprecationMessage = getShowDeprecationMessage(displayNameOrShowMessageFn || fn.name || 'this function', alternativeName, sinceVersion, removeInVersion, stackDepthOffset);
        return function() {
            showDeprecationMessage();
            return fn.apply(this, arguments);
        };
    }

    /**
     * Returns a wrapped version of the constructor that logs a deprecation warning when the constructor is instantiated.
     * @param {Function} constructorFn the constructor function to wrap
     * @param {string|Function} displayNameOrShowMessageFn the name of the fn to be displayed in the message. Alternatively, a function to log deprecation warnings
     * @param {string} alternativeName the name of an alternative function to use
     * @param {string} sinceVersion the version this has been deprecated since
     * @param {string} removeInVersion the version this will be removed in
     * @return {Function} wrapping the original function
     */
    function deprecateConstructor(constructorFn, displayNameOrShowMessageFn, alternativeName, sinceVersion, removeInVersion) {
        var deprecatedConstructor = deprecateFunctionExpression(constructorFn, displayNameOrShowMessageFn, alternativeName, sinceVersion, removeInVersion, 1);
        deprecatedConstructor.prototype = constructorFn.prototype;
        $.extend(deprecatedConstructor, constructorFn); //copy static methods across;

        return deprecatedConstructor;
    }


    var supportsProperties = false;
    try {
        if (Object.defineProperty) {
            Object.defineProperty({}, 'blam', { get : function() {}, set: function() {} });
            supportsProperties = true;
        }
    } catch(e) {
        /* IE8 doesn't support on non-DOM elements */
    }

    /**
     * Wraps a "value" object property in a deprecation warning in browsers supporting Object.defineProperty
     * @param {Object} obj the object containing the property
     * @param {string} prop the name of the property to deprecate
     * @param {string|Function} displayNameOrShowMessageFn the display name of the property to deprecate (optional, will fall back to the property name). Alternatively, a function to log deprecation warnings
     * @param {string} alternativeName the name of an alternative to use
     * @param {string} sinceVersion the version this has been deprecated since
     * @param {string} removeInVersion the version this will be removed in
     */
    function deprecateValueProperty(obj, prop, displayNameOrShowMessageFn, alternativeName, sinceVersion, removeInVersion) {
        if (supportsProperties) {
            var oldVal = obj[prop];
            var showDeprecationMessage = getShowDeprecationMessage(displayNameOrShowMessageFn || prop, alternativeName, sinceVersion, removeInVersion, 1);
            Object.defineProperty(obj, prop, {
                get : function () {
                    showDeprecationMessage();
                    return oldVal;
                },
                set : function(val) {
                    oldVal = val;
                    showDeprecationMessage();
                    return val;
                }
            });
        } else {
            // Browser doesn't support properties, so we can't hook in to show the deprecation warning.
        }
    }

    /**
     * Wraps an object property in a deprecation warning, if possible. functions will always log warnings, but other
     * types of properties will only log in browsers supporting Object.defineProperty
     * @param {Object} obj the object containing the property
     * @param {string} prop the name of the property to deprecate
     * @param {string|Function} displayNameOrShowMessageFn the display name of the property to deprecate (optional, will fall back to the property name). Alternatively, a function to log deprecation warnings
     * @param {string} alternativeName the name of an alternative to use
     * @param {string} sinceVersion the version this has been deprecated since
     * @param {string} removeInVersion the version this will be removed in
     */
    function deprecateObjectProperty(obj, prop, displayNameOrShowMessageFn, alternativeName, sinceVersion, removeInVersion) {
        if (typeof obj[prop] === 'function') {
            obj[prop] = deprecateFunctionExpression(obj[prop], displayNameOrShowMessageFn || prop, alternativeName, sinceVersion, removeInVersion, 1);
        } else {
            deprecateValueProperty(obj, prop, displayNameOrShowMessageFn, alternativeName, sinceVersion, removeInVersion);
        }
    }

    function deprecateAllProperties(obj, objDisplayPrefix, alternativeNamePrefix, sinceVersion, removeInVersion) {
        for(var attr in obj) {
            if (has.call(obj, attr)) {
                deprecateObjectProperty(obj, attr, objDisplayPrefix + attr, alternativeNamePrefix && (alternativeNamePrefix + attr),
                    sinceVersion, removeInVersion);
            }
        }
    }

    window.AJS = window.AJS || [];
    window.AJS.deprecate = {
        fn : deprecateFunctionExpression,
        construct : deprecateConstructor,
        prop : deprecateObjectProperty,
        obj : deprecateAllProperties,
        propertyDeprecationSupported : supportsProperties,
        getMessageLogger : getShowDeprecationMessage
    };

})(jQuery || Zepto);