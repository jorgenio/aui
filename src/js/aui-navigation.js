(function ($) {

    'use strict';

    /**
     * Navigation (".aui-nav" elements).
     *
     * @param {(string|HtmlElement|jQuery)} selector - An expression
     *     representing an .aui-nav element; you may also pass an expression
     *     for a descendent element, in which case the closest containing
     *     .aui-nav element is used.
     * @constructor
     */
    function Navigation(selector) {
        this.$el = $(selector).closest('.aui-nav');
        this.$treeParent = this.$el.parent('li[aria-expanded]');
        this.$subtreeToggleIcon = this.$treeParent
            .children('.aui-nav-subtree-toggle')
            .children('span.aui-icon');
    }

    Navigation.prototype.isNested = function () {
        return this.$treeParent.length === 1;
    };

    Navigation.prototype.isCollapsed = function () {
        return this.$treeParent.attr('aria-expanded') === 'false';
    };

    Navigation.prototype.expand = function () {
        this.$treeParent.attr('aria-expanded', 'true');
        this.$subtreeToggleIcon
            .removeClass('aui-iconfont-collapsed')
            .addClass('aui-iconfont-expanded');
        return this;
    };

    Navigation.prototype.collapse = function () {
        this.$treeParent.attr('aria-expanded', 'false');
        this.$subtreeToggleIcon
            .removeClass('aui-iconfont-expanded')
            .addClass('aui-iconfont-collapsed');
        return this;
    };

    Navigation.prototype.toggle = function () {
        if (this.isCollapsed()) {
            this.expand();
        } else {
            this.collapse();
        }
        return this;
    };


    AJS.navigation = AJS._internal.widget('navigation', Navigation);


    $(function ($) {
        $(document).on('click', '.aui-nav li[aria-expanded]', function (e) {
            var $target = $(e.target);
            var isBubbledToAncestor = $target.closest('li[aria-expanded]')[0] !== this;
            if (isBubbledToAncestor) {
                return;
            }

            var isSubtreeToggleClick = $target.closest('.aui-nav-subtree-toggle', this).length > 0;
            if (isSubtreeToggleClick) {
                e.preventDefault();
            }

            var nav = AJS.navigation($(this).children('.aui-nav'));
            nav.toggle();
        });
    });

})(AJS.$);
