(function($, skate, Tether) {

    var EVENT_PREFIX = "_aui-internal-layer-";
    var GLOBAL_EVENT_PREFIX = "_aui-internal-layer-global-";

    /*
    skate('.aui-layer', {
        insert: function() {
            if (this.getAttribute('data-aui-alignment')) {
                createTether(this);
            }
        }
    });
    */

    function Layer(selector) {
        this.$el = $(selector || '<div class="aui-layer" aria-hidden="true"></div>');
    }

    Layer.prototype.changeSize = function(width, height) {
        this.$el.css('width', width);
        this.$el.css('height', height === 'content' ? '' : height);
        return this;
    };

    Layer.prototype.on = function(event, fn) {
        this.$el.on(EVENT_PREFIX + event, fn);
        return this;
    };

    Layer.prototype.off = function(event, fn) {
        this.$el.off(EVENT_PREFIX + event, fn);
        return this;
    };

    Layer.prototype.show = function() {
        if (this.$el.is(':visible')) {
            return this;
        }
        var e = AJS.$.Event(EVENT_PREFIX + "beforeShow");
        this.$el.trigger(e);

        var e2 = AJS.$.Event(GLOBAL_EVENT_PREFIX + "beforeShow");
        $(document).trigger(e2,[this.$el]);
        if (!e.isDefaultPrevented() && !e2.isDefaultPrevented()) {
            AJS.LayerManager.global.push(this.$el);
            this.$el.attr('data-aui-alignment') && this.$el[0]._tether.enable();
        }
        return this;
    };

    Layer.prototype.hide = function() {
        if (!this.$el.is(':visible')) {
            return this;
        }
        var e = AJS.$.Event(EVENT_PREFIX + "beforeHide");
        this.$el.trigger(e);

        var e2 = AJS.$.Event(GLOBAL_EVENT_PREFIX + "beforeHide");
        $(document).trigger(e2,[this.$el]);
        if (!e.isDefaultPrevented() && !e2.isDefaultPrevented()) {
            AJS.LayerManager.global.popUntil(this.$el);
        }
        return this;
    };

    Layer.prototype.isVisible = function() {
        return this.$el.attr("aria-hidden") === "false";
    };

    Layer.prototype.remove = function() {
        this.hide();
        this.$el[0]._tether && this.$el[0]._tether.destroy();
        this.$el.remove();
        this.$el = null;
    };

    Layer.prototype._showLayer = function(zIndex) {
        if (!this.$el.parent().is('body')) {
            this.$el.appendTo(document.body);
        }
        // inverse order to hideLayer
        this.$el.data('_aui-layer-cached-z-index', this.$el.css('z-index'));
        this.$el.css("z-index", zIndex);
        this.$el.attr("aria-hidden", "false");
        AJS.FocusManager.global.enter(this.$el);
        this.$el.trigger(EVENT_PREFIX + "show");
        $(document).trigger(GLOBAL_EVENT_PREFIX + "show", [this.$el]);
    };

    Layer.prototype._hideLayer = function() {
        // inverse order to showLayer
        AJS.FocusManager.global.exit(this.$el);
        this.$el.attr("aria-hidden", "true");
        this.$el.css('z-index', this.$el.data('_aui-layer-cached-z-index') || '');
        this.$el.data('_aui-layer-cached-z-index', '');
        this.$el.trigger(EVENT_PREFIX + "hide");
        $(document).trigger(GLOBAL_EVENT_PREFIX + "hide", [this.$el]);
    };

    Layer.prototype.isBlanketed = function() {
        return this.$el.attr("data-aui-blanketed") === "true";
    };

    Layer.prototype.isModal = function() {
        return this.$el.attr("data-aui-modal") === "true";
    };

    AJS.layer = AJS._internal.widget('layer', Layer);

    AJS.layer.on = function(eventName, fn) {
        $(document).on(GLOBAL_EVENT_PREFIX + eventName, fn);
        return this;
    };

    AJS.layer.off = function(eventName, fn) {
        $(document).off(GLOBAL_EVENT_PREFIX + eventName, fn);
        return this;
    };

    /*
    var attachmentMap = {
        'top left': { el:'bottom left', target: 'top left' },
        'top center': { el: 'bottom center', target: 'top center' },
        'top right': { el: 'bottom right', target: 'top right' },
        'right top': { el: 'top left', target: 'top right' },
        'right middle': { el: 'middle left', target: 'middle right' },
        'right bottom': { el: 'bottom left', target: 'bottom right' },
        'bottom left': { el: 'top left', target: 'bottom left' },
        'bottom center': { el: 'top center', target: 'bottom center' },
        'bottom right': { el: 'top right', target: 'bottom right' },
        'left top': { el: 'top right', target: 'top left' },
        'left middle': { el: 'middle right', target: 'middle left' },
        'left bottom': { el: 'bottom right', target: 'bottom left' }
    }

    function createTether(layer) {
        var alignment = layer.getAttribute('data-aui-alignment');
        var attachment = attachmentMap[alignment] || { el: 'top center', target: 'bottom center' };
        layer._tether = new Tether({
            element: '#' + layer.id,
            target: '[aria-controls="' + layer.id + '"]',
            attachment: attachment.el,
            targetAttachment: attachment.target,
            enabled: false,
            constraints: [
                {
                    // Try and keep the element on page
                    to: 'window',
                    attachment: 'together',
                    pin: true
                }
            ]
        });
    }
    */
}(AJS.$));
//}(AJS.$, window.skate || require('skate'), window.Tether || require('tether')));
