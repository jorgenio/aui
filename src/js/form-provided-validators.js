(function ($) {
    'use strict';

    //Input length
    AJS.validator.register(['maxlength', 'minlength'], function(field) {
        var minlengthMessage = makeMessage('minlength', field.args,
            AJS.I18n.getText('aui.validation.message.minlength'));
        var maxlengthMessage = makeMessage('maxlength', field.args,
            AJS.I18n.getText('aui.validation.message.maxlength'));

        if (field.$el.val().length < field.args('minlength')){
            field.invalidate(minlengthMessage);
        } else if (field.$el.val().length > field.args('maxlength')){
            field.invalidate(maxlengthMessage);
        } else {
            field.validate();
        }
    });

    //Field matching
    AJS.validator.register(['matchingfield'], function(field){
        var thisFieldValue = field.$el.val();
        var $matchingField = $('#' + field.args('matchingfield'));
        var matchingFieldValue = $matchingField.val();

        var matchingFieldMessage = makeMessage('matchingfield', field.args,
            AJS.I18n.getText('aui.validation.message.matchingfield'), [thisFieldValue, matchingFieldValue]);

        if (!thisFieldValue || !matchingFieldValue){
            field.validate();
        } else if (matchingFieldValue !== thisFieldValue) {
            field.invalidate(matchingFieldMessage);
        } else {
            field.validate();
        }
    });

    //Banned words
    AJS.validator.register(['doesnotcontain'], function(field) {
        var doesNotContainMessage = makeMessage('doesnotcontain', field.args,
            AJS.I18n.getText('aui.validation.message.doesnotcontain'));

        if(field.$el.val().indexOf(field.args('doesnotcontain')) === -1) {
            field.validate();
        } else {
            field.invalidate(doesNotContainMessage);
        }
    });

    //Matches regex
    AJS.validator.register(['pattern'], function(field) {
        var patternMessage = makeMessage('pattern', field.args,
            AJS.I18n.getText('aui.validation.message.pattern'));

        if(matchesRegex(field.$el.val(), new RegExp(field.args('pattern'), 'i'))) {
            field.validate();
        } else {
            field.invalidate(patternMessage);
        }
    });

    //Required field
    AJS.validator.register(['required'], function(field) {
        var requiredMessage = makeMessage('required', field.args,
            AJS.I18n.getText('aui.validation.message.required'));

        if (field.$el.val()) {
            field.validate();
        } else {
            field.invalidate(requiredMessage);
        }
    });

    //Field value range (between min and max)
    AJS.validator.register(['min', 'max'], function(field) {
        var validNumberMessage = makeMessage('validnumber', field.args,
            AJS.I18n.getText('aui.validation.message.validnumber'));
        var belowMinMessage = makeMessage('min', field.args,
            AJS.I18n.getText('aui.validation.message.min'));
        var aboveMaxMessage = makeMessage('max', field.args,
            AJS.I18n.getText('aui.validation.message.max'));

        var fieldValue = parseInt(field.$el.val());
        if (isNaN(fieldValue)) {
            field.invalidate(validNumberMessage);
            return;
        }
        if (field.args('min') && (fieldValue < parseInt(field.args('min')))) {
            field.invalidate(belowMinMessage);
        } else if(field.args('max') && (fieldValue > parseInt(field.args('max')))){
            field.invalidate(aboveMaxMessage);
        } else {
            field.validate();
        }
    });

    //Date format
    AJS.validator.register(['dateformat'], function(field) {
        var dateFormatSymbolic = field.args('dateformat');
        var dateFormatMessage = makeMessage('dateformat', field.args,
            AJS.I18n.getText('aui.validation.message.dateformat'));

        var symbolRegexMap = {
            'Y': '[0-9]{4}',
            'y': '[0-9]{2}',
            'm': '(11|12|0{0,1}[0-9])',
            'M': '[Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec]',
            'D': '[Mon|Tue|Wed|Thu|Fri|Sat|Sun]',
            'd': '([0-2]{0,1}[0-9]{1})|(30|31)'
        };

        var dateFormatSymbolArray = dateFormatSymbolic.split('');
        var dateFormatRegexString = '';

        dateFormatSymbolArray.forEach(function(dateSymbol) {
            var isRecognisedSymbol = symbolRegexMap.hasOwnProperty(dateSymbol);
            if (isRecognisedSymbol) {
                dateFormatRegexString += symbolRegexMap[dateSymbol];
            } else {
                dateFormatRegexString += dateSymbol;
            }
        });

        var dateFormatRegex = new RegExp(dateFormatRegexString+'$', 'i');
        var isValidDate = matchesRegex(field.$el.val(), dateFormatRegex);

        if (isValidDate) {
            field.validate();
        } else {
            field.invalidate(dateFormatMessage);
        }
    });

    //Checkbox count
    AJS.validator.register(['minchecked', 'maxchecked'], function(field) {
        var amountChecked = field.$el.find(':checked').length;
        var aboveMin = !field.args('minchecked') || (amountChecked >= field.args('minchecked'));
        var belowMax = !field.args('maxchecked') || (amountChecked <= field.args('maxchecked'));

        var belowMinMessage = makeMessage('minchecked', field.args,
            AJS.I18n.getText('aui.validation.message.minchecked'));
        var aboveMaxMessage = makeMessage('maxchecked', field.args,
            AJS.I18n.getText('aui.validation.message.maxchecked'));

        if (aboveMin && belowMax) {
            field.validate();
        } else if (!aboveMin) {
            field.invalidate(belowMinMessage);
        } else if (!belowMax) {
            field.invalidate(aboveMaxMessage);
        }
    });

    /*
         Retrieves a message for a plugin validator through the data attributes or the default (which is in the i18n file)
         The argument AJS.I18n.getText('aui.validation.message...') (defaultMessage) cannot be refactored as it
         must appear verbatim for the plugin I18n transformation to pick it up
     */
    function makeMessage(key, accessorFunction, defaultMessage, tokenReplacements) {

        var inFlatpackMode = AJS.I18n.keys !== undefined;
        if (inFlatpackMode) {
            defaultMessage = AJS.I18n.keys['aui.validation.message.' + key];
        }

        if (!tokenReplacements) {
            tokenReplacements = [accessorFunction(key)]; //if no token is specified, pass in the argument as the token
        }
        var customMessageUnformatted = accessorFunction(key+'-msg');
        var formattingArguments;

        if (customMessageUnformatted) {
            formattingArguments = [customMessageUnformatted].concat(tokenReplacements);
        } else {
            formattingArguments = [defaultMessage].concat(tokenReplacements);
        }

        return AJS.format.apply(null, formattingArguments);
    }

    function matchesRegex(val, regex){
        var matches = val.match(regex);
        if (!matches) {
            return false;
        }
        var isExactMatch = (val === matches[0]);
        return isExactMatch;
    }

})(AJS.$);