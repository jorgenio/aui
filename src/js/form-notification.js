(function ($) {
    'use strict';

    var NOTIFICATION_NAMESPACE = 'aui-form-notification';

    var ATTRIBUTE_TOOLTIP_MESSAGE = NOTIFICATION_NAMESPACE + '-message';
    var ATTRIBUTE_TOOLTIP_POSITION = NOTIFICATION_NAMESPACE + '-position';
    var ATTRIBUTE_TOOLTIP_ACTIVE_ICON = NOTIFICATION_NAMESPACE + '-active-icon';
    var ATTRIBUTE_TOOLTIP_NOTIFICATION_TYPE = NOTIFICATION_NAMESPACE + '-type';

    var CLASS_FIELD_TOOLTIP_INITIALISED = NOTIFICATION_NAMESPACE + '-initialised';

    var CLASS_ICON_ERROR = NOTIFICATION_NAMESPACE + '-icon-error';
    var CLASS_ICON_INFO = NOTIFICATION_NAMESPACE + '-icon-info';
    var ALL_ICON_CLASSES = [CLASS_ICON_ERROR, CLASS_ICON_INFO];

    var CLASS_NOTIFICATION_ICON = 'aui-icon-notification';

    var CLASS_TOOLTIP = NOTIFICATION_NAMESPACE + '-tooltip';
    var CLASS_TOOLTIP_ERROR = CLASS_TOOLTIP + '-error';
    var CLASS_TOOLTIP_INFO = CLASS_TOOLTIP + '-info';

    var ICON_ERROR = 'aui-iconfont-error';
    var ICON_WAIT = 'aui-icon-wait';
    var ICON_INFO = 'aui-iconfont-info';

    /* --- Tipsy configuration --- */
    var TIPSY_OPACITY = 1;
    var TIPSY_OFFSET_INSIDE_FIELD = 7; //offset in px from the icon to the start of the tipsy
    var TIPSY_OFFSET_OUTSIDE_FIELD = 3;


    AJS._notifyField = notifyField;

    /**
     * Decorate a field with a tipsy and an icon. Sending an empty options.type means no decoration, and sending an
     * empty options.message clears the stack of messages
     * @param $field the field to decorate
     * @param options some options to decorate the field with (type specifies the icon / classes, message is the tipsy class)
     */
    function notifyField($field, options) {
        if (!options) {
            console.warn('Attempting to notify a field without options');
            return;
        }

        var type = options.type;
        var message = options.message;

        if (message) {
            addMessageToField($field, message);
        } else {
            clearFieldMessages($field);
        }

        initialiseNotification($field);

        setFieldIconForType($field, type);

        //Add a data attribute to keep track of the notification type. Tipsy needs this to get the tipsy class at show time
        setFieldNotificationType($field, type);

        //Normally visibility is toggled by focus in/out events, but if the field is already active, start visible
        var tooltipStartsVisible = (isFieldFocusable($field) && isFieldActive($field)) || !isFieldFocusable($field);
        if (tooltipStartsVisible) {
            setTooltipVisible($field);
        }

    }

    function initialiseNotification($field) {
        if (!isFieldInitialised($field)) {
            prepareFieldMarkup($field);
            initialiseTooltip($field);
            bindFieldEvents($field);
        }
    }

    function getFieldNotificationClass($field) {
        var type = getFieldNotificationType($field);
        var typeToClassMap = {
            error: CLASS_TOOLTIP_ERROR,
            info: CLASS_TOOLTIP_INFO
        };
        return typeToClassMap[type];
    }

    function getFieldNotificationType($field) {
        return $field.data(ATTRIBUTE_TOOLTIP_NOTIFICATION_TYPE);
    }

    function setFieldNotificationType($field, type) {
        $field.data(ATTRIBUTE_TOOLTIP_NOTIFICATION_TYPE, type);
    }

    function prepareFieldMarkup($field) {
        $field.addClass(CLASS_FIELD_TOOLTIP_INITIALISED);
        appendIconToField($field);
    }

    function appendIconToField($field) {
        var $icon = constructFieldIcon();
        $field.after($icon);

        if (canContainIcon($field)){
            $field.addClass('aui-field-has-invisible-icon');
        } else {
            $icon.addClass('aui-form-notification-icon-outside-field');
        }
    }

    function initialiseTooltip($field) {
        getTooltipAnchor($field).tipsy({
            gravity: getTipsyGravity($field),
            title: function(){
                return getFormattedFieldMessage($field);
            },
            trigger: 'manual',
            offset: canContainIcon($field) ? TIPSY_OFFSET_INSIDE_FIELD : TIPSY_OFFSET_OUTSIDE_FIELD,
            opacity: TIPSY_OPACITY,
            className: function() {
                return CLASS_TOOLTIP + ' ' + getFieldNotificationClass($field);
            },
            html: true
        });
    }

    function getFormattedFieldMessage($field) {
        var messages = getFieldMessages($field);
        if (messages.length === 0) {
            return '';
        } else if(messages.length === 1) {
            return messages[0];
        }

        var wrappedListItems = messages.map(function(message) {
            return '<li>' + message + '</li>';
        });
        return '<ul>' + wrappedListItems.join('') + '</ul>';
    }

    function getFieldMessages($field) {
        var jsonMessages = $field.data(ATTRIBUTE_TOOLTIP_MESSAGE);
        return jsonMessages ? JSON.parse(jsonMessages) : [];
    }

    function setFieldMessages($field, messages) {
        var jsonMessages = JSON.stringify(messages);
        $field.data(ATTRIBUTE_TOOLTIP_MESSAGE, jsonMessages);
    }

    function bindFocusShowsTooltipEvent($field) {
        $field.on('focusin', function(){
            setTooltipVisible($field);
        });

        $field.on('focusout', function() {
            setTooltipInvisible($field);
        });
    }

    function setFieldIconForType($field, type) {
        var typeToIconMap = {
            error: {
                icon: ICON_ERROR,
                iconClass: CLASS_ICON_ERROR
            },
            wait: {
                icon: ICON_WAIT
            },
            info: {
                icon: ICON_INFO,
                iconClass: CLASS_ICON_INFO
            },
            none: {
                icon: '',
                iconClass: ''
            }
        };

        if (!typeToIconMap[type]) {
            console.warn('Setting field icon for unknown type: ' + type);
        }

        var icon = typeToIconMap[type].icon;
        var iconClass = typeToIconMap[type].iconClass || '';


        setFieldIcon($field, icon);
        setFieldIconClass($field, iconClass);
    }

    function clearFieldMessages($field) {
        setTooltipInvisible($field);
        $field.data(ATTRIBUTE_TOOLTIP_MESSAGE, '');
    }

    function isFieldFocusable($field) {
        return $field.is(':aui-focusable');
    }

    function isFieldActive($field) {
        return $field.is(document.activeElement);
    }

    function isFieldInitialised($field) {
        return $field.hasClass(CLASS_FIELD_TOOLTIP_INITIALISED);
    }

    function bindFieldEvents($field) {
        if (focusTogglesTooltip($field)) {
            bindFocusShowsTooltipEvent($field);
        }
    }

    function constructFieldIcon(){
        return $('<span class="aui-icon aui-icon-small ' + CLASS_NOTIFICATION_ICON + '"/>');
    }

    function focusTogglesTooltip($field) {
        return $field.is(':aui-focusable');
    }

    function setTooltipVisible($field) {
        getTooltipAnchor($field).tipsy('show');

    }

    function setTooltipInvisible($field) {
        getTooltipAnchor($field).tipsy('hide');
    }

    function getTipsyGravity($field) {
        var position = $field.data(ATTRIBUTE_TOOLTIP_POSITION) || 'side';
        var gravityMap = {
            side: 'w',
            top: 'se',
            bottom: 'ne'
        };
        var gravity = gravityMap[position];
        if (!gravity) {
            gravity = 'w';
            throw new Error('Invalid notification position: "'+position+'". Valid options are "side", "bottom, "top"');
        }
        return gravity;
    }

    function canContainIcon($field) {
        var isTextOrPassword = ['text', 'password'].indexOf($field.attr('type')) !== -1;
        return isTextOrPassword;
    }

    function addMessageToField($field, newMessage) {
        if (!newMessage) {
            return;
        }
        var oldMessages = getFieldMessages($field);
        var combinedMessages = oldMessages.concat([newMessage]);
        setFieldMessages($field, combinedMessages);
    }

    function getTooltipAnchor($field){
        return getFieldIcon($field);
    }

    function setFieldIconClass($field, newClass) {
        var $icon = getFieldIcon($field);
        removeAllIconClasses($icon);
        addClassToIcon($icon, newClass);
    }

    function removeAllIconClasses($icon) {
        var allClasses = ALL_ICON_CLASSES.join(' ');
        $icon.removeClass(allClasses);
    }

    function addClassToIcon($icon, classToAdd) {
        $icon.addClass(classToAdd);
    }

    function setFieldIcon($field, newIcon) {
        replaceIcon($field, newIcon);

        if (newIcon) {
            setFieldIconVisible($field);
        } else {
            setFieldIconInvisible($field);
        }
    }

    function replaceIcon($field, newIcon) {
        var oldIcon = $field.data(ATTRIBUTE_TOOLTIP_ACTIVE_ICON);
        $field.data(ATTRIBUTE_TOOLTIP_ACTIVE_ICON, newIcon);

        var $icon = getFieldIcon($field);
        if (oldIcon) {
            $icon.removeClass(oldIcon);
        }
        $icon.addClass(newIcon);
    }

    function setFieldIconVisible($field) {
        if (canContainIcon($field)) {
            $field.removeClass('aui-field-has-invisible-icon');
            $field.addClass('aui-field-has-icon');
        } else {
            getFieldIcon($field).removeClass('hidden');
        }
    }

    function setFieldIconInvisible($field) {
        if (canContainIcon($field)) {
            $field.addClass('aui-field-has-invisible-icon');
            $field.removeClass('aui-field-has-icon');
        } else {
            getFieldIcon($field).addClass('hidden');
        }
    }

    function getFieldIcon($field){
        return $field.next('.' + CLASS_NOTIFICATION_ICON);
    }

})(AJS.$);