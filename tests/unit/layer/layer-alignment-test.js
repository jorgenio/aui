define(['skate', 'layer', 'aui-qunit', 'tether'], function(skate) {
    /*
    function createLayer(id, alignment) {
        var $el = AJS.$('<div id="layer-1" class="aui-layer"></div>').attr('data-aui-alignment', alignment).attr('aria-hidden', true).appendTo('#canvas');

        //layer needs some height so that layer :visible selector works properly
        $el.height(16);
        $el.width(16);
        skate($el);

        return $el;
    }

    function comparePosition(position, top, left) {
        return (position.top === top) && (position.left === left);
    }

    module("Layer tests", {
        setup: function() {
            var $el = AJS.$('<div id="canvas" style="position:absolute;"></div>').appendTo("#qunit-fixture");
            $el.height(100);
            $el.width(100);
            $el.css({top: 0, left: 0});

            this.$anchor = AJS.$('<div aria-controls="layer-1" style="position:relative;">Im the target</div>').appendTo('#canvas');
            this.$anchor.css({top: 40, left: 40}); // throw it in the middle of the canvas
            this.$anchor.height(10);
            this.$anchor.width(10);
        },
        teardown: function() {
            AJS.qunit.removeLayers();
        }
    });

    test("Layer alignment", function() {
        var expectedOffsets = [
            {position: '', x: 100, y: 8},
            {position: 'incorrect alignment', x: 50, y: 37},

            {position: 'bottom right', x: 50, y: 34},
            {position: 'bottom center', x: 50, y: 37},
            {position: 'bottom left', x: 50, y: 40},

            {position: 'top left', x: 24, y: 40},
            {position: 'top center', x: 24, y: 37},
            {position: 'top right', x: 24, y: 34},

            {position: 'right top', x: 40, y: 50},
            {position: 'right middle', x: 37, y: 50},
            {position: 'right bottom', x: 34, y: 50},

            {position: 'left top', x: 40, y: 24},
            {position: 'left middle', x: 37, y: 24},
            {position: 'left bottom', x: 34, y: 24}
        ];

        expectedOffsets.forEach(function(expected) {
            var $layer = createLayer('layer-1', expected.position);
            AJS.layer($layer).show();

            ok(comparePosition($layer.offset(), expected.x, expected.y), expected);

            AJS.layer($layer).remove();
        });
    });
    */
});