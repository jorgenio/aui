define(['dropdown', 'aui-qunit'], function() {
    module("Dropdown Unit Tests", {
        setup: function () {
            AJS.$('#qunit-fixture').html(
                '<ul id="dropdown-test">' +
                    '<li class="aui-dd-parent">' +
                    '<a href="#" class="aui-dd-trigger">A Test Dropdown</a>' +
                        '<ul class="aui-dropdown">' +
                            '<li class="dropdown-item"><a href="#" class="item-link">Link 1</a></li>' +
                            '<li class="dropdown-item"><a href="#" class="item-link">Link 2</a></li>' +
                            '<li class="dropdown-item"><a href="#" class="item-link">Link 3</a></li>' +
                        '</ul>' +
                    '</li>' +
                '</ul>'
            );
            this.dropdown = AJS.$("#dropdown-test").dropDown("Standard")[0];
        },
        teardown: function () {
            this.dropdown.hide();
        }
    });

    test("Dropdown Creation", function() {
        var testDropdown = AJS.dropDown("test","standard");
        ok(typeof testDropdown == "object", "dropdown object was created successfully!");
    });

    test("Dropdown move down", function() {
        AJS.$("#dropdown-test .aui-dd-trigger").click();
        this.dropdown.moveDown();
        var dropdownItems = AJS.$(".dropdown-item"),
            selectedItem = AJS.$(".dropdown-item.active");
        equal(selectedItem.length, 1, "Only one item selected");
        equal(selectedItem[0], dropdownItems[0], "First item selected");

        this.dropdown.cleanActive();
        this.dropdown.moveDown();
        selectedItem = AJS.$(".dropdown-item.active");
        equal(selectedItem.length, 1, "Only one item selected");
        equal(selectedItem[0], dropdownItems[1], "Second item selected");

        this.dropdown.cleanActive();
        this.dropdown.moveDown();
        selectedItem = AJS.$(".dropdown-item.active");
        equal(selectedItem.length, 1, "Only one item selected");
        equal(selectedItem[0], dropdownItems[2], "Third item selected");

        this.dropdown.cleanActive();
        this.dropdown.moveDown();
        selectedItem = AJS.$(".dropdown-item.active");
        equal(selectedItem.length, 1, "Only one item selected");
        equal(selectedItem[0], dropdownItems[0], "First item selected again");
    });

    test("Dropdown move up", function() {
        AJS.$("#dropdown-test .aui-dd-trigger").click();
        this.dropdown.moveUp();
        var dropdownItems = AJS.$(".dropdown-item"),
            selectedItem = AJS.$(".dropdown-item.active");
        equal(selectedItem.length, 1, "Only one item selected");
        equal(selectedItem[0], dropdownItems[2], "Third item selected");

        this.dropdown.cleanActive();
        this.dropdown.moveUp();
        selectedItem = AJS.$(".dropdown-item.active");
        equal(selectedItem.length, 1, "Only one item selected");
        equal(selectedItem[0], dropdownItems[1], "Second item selected");

        this.dropdown.cleanActive();
        this.dropdown.moveUp();
        selectedItem = AJS.$(".dropdown-item.active");
        equal(selectedItem.length, 1, "Only one item selected");
        equal(selectedItem[0], dropdownItems[0], "First item selected");

        this.dropdown.cleanActive();
        this.dropdown.moveUp();
        selectedItem = AJS.$(".dropdown-item.active");
        equal(selectedItem.length, 1, "Only one item selected");
        equal(selectedItem[0], dropdownItems[2], "Third item selected again");
    });
});