(function() {
    'use strict';

    function addFixtureToBody () {
        AJS.$('<div id="qunit-fixture"></div>').appendTo(document.body);
    }

    function removeFixtureFromBody () {
        AJS.$('#qunit-fixture').remove();
    }

    function getLayers () {
        return AJS.$('.aui-layer');
    }

    function warnIfLayersExist(test) {
        if (getLayers().length) {
            // Need to bind to console for chrome, otherwise it throws an illegal invocation error.
            console.log(
                test.module +
                    ': ' +
                    test.name +
                    ' - ' +
                    'Layers have been left in the DOM. These must be removed from the BODY to ensure they don\'t affect other tests. Use AJS.qunit.removeLayers().'
            );
            removeLayers();
        }
    }


    function createFixtureItems (fixtureItems, removeOldFixtures) {
        var fixtureElement = document.getElementById('qunit-fixture');

        if (removeOldFixtures || removeOldFixtures === undefined) {
            fixtureElement.innerHTML = '';
        }

        if (fixtureItems) {
            for (var name in fixtureItems) {
                fixtureItems[name] = $(fixtureItems[name]).get(0);
                fixtureElement.appendChild(fixtureItems[name]);
            }
        }

        return fixtureItems;
    }

    function removeLayers () {
        var $layer;

        while ($layer = AJS.LayerManager.global.getTopLayer()) {
            AJS.LayerManager.global.popUntil($layer);
        }

        getLayers().remove();
    }


    // Karma does not create a qunit-fixture element by default - use this to setup and teardown the fixture.
    QUnit.testStart(function (test) {
        addFixtureToBody();
    });

    QUnit.testDone(function (test) {
        warnIfLayersExist(test);
        removeFixtureFromBody();
    });

    AJS.qunit = {
        fixtures: createFixtureItems,
        removeLayers: removeLayers
    };

})();
