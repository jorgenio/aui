define(['jquery', 'skate', 'trigger', 'aui-qunit'], function($, skate) {
    'use strict';

    var element;
    var trigger;
    var disabledTrigger;

    var componentAttributeFlag = 'data-my-component';
    var componentId = 'my-element';

    function click(trigger) {
        trigger.dispatchEvent(new CustomEvent('click',{
            cancelable: true
        }));
    }

    function createComponent() {
        return skate(componentAttributeFlag, {
            type: skate.types.ATTR,
            prototype: {
                show: function() {
                    this.style.display = 'block';
                    this.dispatchEvent(new CustomEvent('aui-after-show'));
                },
                hide: function() {
                    this.style.display = 'none';
                    this.dispatchEvent(new CustomEvent('aui-after-hide'));
                },
                isVisible: function() {
                    return this.style.display === 'block';
                }
            }
        });
    }

    createComponent();

    function createElement() {
        var el = document.createElement('div');
        $(el)
            .text('some content')
            .attr('id', componentId)
            .attr(componentAttributeFlag, '')
            .css({
                display: 'none',
                height: 100,
                width: 100
            })
            .appendTo('#qunit-fixture');
        skate.init(el);

        return el;
    }

    function triggerFactory(tag, type, attributes) {
        var el = document.createElement(tag);

        if (attributes && typeof attributes === 'object') {
            for(var prop in attributes) {
                el.setAttribute(prop, attributes[prop]);
            }
        }

        el.setAttribute('data-aui-trigger', type);
        el.setAttribute('aria-controls', componentId);
        document.getElementById('qunit-fixture').appendChild(el);

        skate.init(el);

        return el;
    }

    function createButtonTrigger(type, attributes) {
        return triggerFactory('button', type, attributes);
    }

    function createAnchorTrigger(type, attributes) {
        return triggerFactory('a', type, attributes);
    }

    function disableTrigger(trigger) {
        trigger.setAttribute('aria-disabled', 'true');
        return trigger;
    }

    module('Trigger - Behaviour', {
        setup: function() {
            element = createElement();
            element.hide(); //make sure the element is hidden initially

            trigger = createButtonTrigger('toggle');
            disabledTrigger = disableTrigger(createButtonTrigger('toggle'));
        },

        teardown: function() {
        }
    });

    test('component should toggle when trigger is clicked', function() {
        ok(!element.isVisible());
        click(trigger);
        ok(element.isVisible());
        click(trigger);
        ok(!element.isVisible());
    });

    test('isDisabled() should return true after aria-disabled="true" is added', function() {
        ok(!trigger.isDisabled());
        disableTrigger(trigger);
        ok(trigger.isDisabled());
    });

    test('isDisabled() should return false when there is no aria-disabled attribute', function() {
        ok(!trigger.isDisabled());
    });

    test('disable() should disable the trigger', function() {
        ok(!trigger.isDisabled());
        trigger.disable();
        ok(trigger.isDisabled());
    });

    test('enable() should enable the trigger', function() {
        ok(!trigger.isDisabled());
        trigger.disable();
        ok(trigger.isDisabled());
        trigger.enable();
        ok(!trigger.isDisabled());
    });

    test('should not toggle when disabled trigger is clicked', function() {
        ok(!element.isVisible());
        click(disabledTrigger);
        ok(!element.isVisible());
        click(disabledTrigger);
        ok(!element.isVisible());
    });

    module('Trigger - Elements', {
        setup: function() {
            element = createElement();
            element.hide(); //make sure the element is hidden initially
            this.dontCallMe = sinon.stub().returns('Wait, no! Don\'t go :(');
            $(window).on('beforeunload', this.dontCallMe);
        },

        teardown: function() {
            $(window).off('beforeunload', this.dontCallMe);
            if (trigger && trigger.remove) { trigger.remove(); }
        }
    });

    test('if a trigger is an anchor, its hyperlink should not be followed', function() {
        trigger = createAnchorTrigger('toggle');
        trigger.setAttribute('href', 'http://google.com/');

        click(trigger);
        equal(this.dontCallMe.callCount, 0, 'should not follow hyperlink');
    });

});
