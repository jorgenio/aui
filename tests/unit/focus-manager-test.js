define(['focus-manager', 'aui-qunit'], function() {
    module("Focus Manager focus tests", {
        createSingleInput: function() {
            this.$el = AJS.$("<input type='text' />").appendTo("#qunit-fixture");
            this.spy = sinon.spy();
            this.$el.focus(this.spy);
        },
        createTwoInputs: function() {
            this.$container = AJS.$("<div></div>").appendTo("#qunit-fixture");
            this.$el1 = AJS.$("<input type='text' />").appendTo(this.$container);
            this.$el2 = AJS.$("<input type='text' />").appendTo(this.$container);
            this.spy1 = sinon.spy();
            this.spy2 = sinon.spy();
            this.$el1.focus(this.spy1);
            this.$el2.focus(this.spy2);
        }
    });

    test("FocusManager.enter() focuses on the first element only using the default selector", function() {
        this.createTwoInputs();

        new AJS.FocusManager().enter(this.$container);

        ok(this.spy1.calledOnce, "First element should have been focussed");
        ok(!this.spy2.called, "Second element should not have been focussed");
    });

    test("FocusManager.enter() does not focus if data-aui-focus=\"false\" is provided", function() {
        this.createTwoInputs();

        this.$container.attr("data-aui-focus", "false");
        new AJS.FocusManager().enter(this.$container);

        ok(!this.spy1.called, "First element should not have been focussed");
        ok(!this.spy2.called, "Second element should not have been focussed");
    });

    test("FocusManager.enter() does not focus if data-aui-focus=\"false\" is provided and data-aui-focus-selector is present", function() {
        this.createTwoInputs();
        this.$el2.attr("id", "sideshow-bob");
        this.$container.attr("data-aui-focus", "false");
        this.$container.attr("data-aui-focus-selector", "#sideshow-bob");

        new AJS.FocusManager().enter(this.$container);

        ok(!this.spy1.called, "First element should not have been focussed");
        ok(!this.spy2.called, "Second element should not have been focussed");
    });

    test("FocusManager.enter() focuses on the specified element using a custom selector", function() {
        this.createTwoInputs();
        this.$el2.attr("id", "sideshow-bob");
        this.$container.attr("data-aui-focus-selector", "#sideshow-bob");

        new AJS.FocusManager().enter(this.$container);

        ok(!this.spy1.called, "First element should not have been focussed");
        ok(this.spy2.calledOnce, "Second element should have been focussed");
    });

    test("FocusManager.enter() focuses on the first element only using a custom selector", function() {
        this.createTwoInputs();
        this.$el2.attr("id", "sideshow-bob");
        this.$container.attr("data-aui-focus-selector", ":input");

        new AJS.FocusManager().enter(this.$container);

        ok(this.spy1.calledOnce, "First element should have been focussed");
        ok(!this.spy2.called, "Second element should not have been focussed");
    });

    test("FocusManager.enter() selects passed element if it matches the focus selector", function() {
        this.createSingleInput();

        new AJS.FocusManager().enter(this.$el);

        ok(this.spy.calledOnce, "Focus listener should have been called on enter");
    });

    module("Focus Manager focus tests", {
        createSingleInput: function() {
            this.$el = AJS.$("<input type='text' />").appendTo("#qunit-fixture");
            this.spy = sinon.spy();
            this.$el.blur(this.spy);
        },
        createTwoInputs: function() {
            this.$container = AJS.$("<div></div>").appendTo("#qunit-fixture");
            this.$el1 = AJS.$("<input type='text' />").appendTo(this.$container);
            this.$el2 = AJS.$("<input type='text' />").appendTo(this.$container);
            this.spy1 = sinon.spy();
            this.spy2 = sinon.spy();
            this.$el1.blur(this.spy1);
            this.$el2.blur(this.spy2);
        }
    });

    test("FocusManager.exit() blurs the active element", function() {
        this.createTwoInputs();
        this.$el1.focus();

        new AJS.FocusManager().exit(this.$container);

        ok(this.spy1.calledOnce, "Expected blur spy to be called");
    });

    test("FocusManager.exit() blurs the active element if the passed element is focussed", function() {
        this.createSingleInput();
        this.$el.focus();

        new AJS.FocusManager().exit(this.$el);

        ok(this.spy.calledOnce, "Expected blur spy to be called");
    });

    test("FocusManager.exit() does not trigger blur on an element that is not underneath it", function() {
        this.createTwoInputs();
        var $el = AJS.$("<input type='text' />").appendTo("#qunit-fixture");
        $el.focus();

        new AJS.FocusManager().exit(this.$container);

        ok(!this.spy1.called);
        ok(!this.spy2.called);
    });

    test('FocusManager preserves focus after enter() then exit()', function() {
        this.createTwoInputs();
        var $focusButton = $('<button id="focusButton">Focus button</button>');
        AJS.$('#qunit-fixture').append($focusButton);
        $focusButton.focus();

        new AJS.FocusManager().enter(this.$container);
        ok(!$focusButton.is(document.activeElement), 'Focus has shifted');
        new AJS.FocusManager().exit(this.$container);

        ok($focusButton.is(document.activeElement), 'FocusManager preserves focus after enter() then exit()');
    });
});
