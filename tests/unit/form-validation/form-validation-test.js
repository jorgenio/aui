define(['skate', 'form-provided-validators', 'aui-qunit'], function(skate) {
    module("Form validation Unit Tests", {
        addInputToElement: function(attributes, $parent, inputHTML) {
            var html = inputHTML ? inputHTML : '<input type="text">';
            var $input = AJS.$(html);

            attributes = $.extend({}, attributes, {'data-aui-field': '', 'data-aui-validate-when': 'keyup'});

            $.each(attributes, function(key, value){
                $input.attr(key, value);
            });
            $parent.append($input);
            skate.init($input);
            return $input;
        },
        typeMessage: function($input, messageToType) {
            $input.val(messageToType);
            $input.trigger('keyup');
            $input.focus();
        },
        fieldIsValid: function($input){
            return $input.attr('data-aui-validate-state') === 'valid';
        },
        fieldIsInvalid: function($input){
            return $input.attr('data-aui-validate-state') === 'invalid';
        },
        fieldIsUnvalidated: function($input){
            return $input.attr('data-aui-validate-state') === 'unvalidated';
        },
        tipsyOnPage: function(){
            return AJS.$('.tipsy').length > 0;
        },
        firstTipsyOnPageMessage: function() {
            return AJS.$('.tipsy').first().text();
        },
        iconsOnPage: function(){
            return AJS.$('.aui-icon').length - AJS.$('.aui-icon.hidden').length - AJS.$('.aui-field-has-invisible-icon').length;
        },
        teardown: function(){
            AJS.$(".tipsy").remove();
        },
        setup: function() {
            try {
                //Register validators for general usage
                AJS.validator.register(['alwaysinvalidate'], function(field) {
                    field.invalidate('Invalid');
                });
            } catch (e) {
                //we expect an error as we try to register this plugin every time setup is called.
                if (e.name !== 'FormValidationPluginError') {
                    throw e;
                }
            }
        }
    });

    test('Input fields start with unvalidated class', function() {
        var $input = this.addInputToElement({
            'data-aui-validate-minlength': '20'
        }, AJS.$('#qunit-fixture'));
        ok(this.fieldIsUnvalidated($input), 'Fields start with unvalidated class');
    });

    test('Input field becomes validated after typing valid message', function() {
        var $input = this.addInputToElement({
            'data-aui-validate-minlength': '20'
        }, AJS.$('#qunit-fixture'));

        this.typeMessage($input, 'This is a message longer than twenty characters');
        ok(this.fieldIsValid($input), 'Input has become valid after typing valid input');
    });

    test('Input field becomes invalid after typing invalid message', function() {
        var $input = this.addInputToElement({
            'data-aui-validate-minlength': '100'
        }, AJS.$('#qunit-fixture'));

        this.typeMessage($input, 'This is a message shorter than one hundred characters');
        ok(this.fieldIsInvalid($input), 'Input has become invalid after typing invalid input');
    });

    test('Input field becomes invalid if one or two validation functions return invalid', function() {
        var $input = this.addInputToElement({
            'data-aui-validate-minlength': '5',
            'data-aui-validate-dateformat': 'Y-m-d'
        }, AJS.$('#qunit-fixture'));

        this.typeMessage($input, 'A');
        ok(this.fieldIsInvalid($input), 'Input has become invalid if two validators invalidate');
        this.typeMessage($input, 'AAAAAAAAAA');
        ok(this.fieldIsInvalid($input), 'Input has become invalid if one validator invalidates');
    });

    test('Tooltip is shown when field is invalidated', function() {
        var $input = this.addInputToElement({
            'data-aui-validate-maxlength': '20'
        }, AJS.$('#qunit-fixture'));

        this.typeMessage($input, 'This is a message longer than twenty characters');
        ok(this.tipsyOnPage(), 'A tooltip is shown when a field becomes invalid');
    });

    test('Icon is shown when icons needing fields are invalidated', function() {
        var $textInput = this.addInputToElement({
            'data-aui-validate-minlength': '20'
        }, AJS.$('#qunit-fixture'), '<input type="text">');
        this.typeMessage($textInput, 'Too short');
        strictEqual(this.iconsOnPage(), 1, 'An icon is shown when a text input becomes invalid');

        this.typeMessage($textInput, 'This is a message longer than twenty characters');
        strictEqual(this.iconsOnPage(), 0, 'No icon is shown when a text input becomes valid');


        var $passwordInput = this.addInputToElement({
            'data-aui-validate-minlength': '20'
        }, AJS.$('#qunit-fixture'), '<input type="password">');
        this.typeMessage($passwordInput, 'Too short');
        strictEqual(this.iconsOnPage(), 1, 'An icon is shown when a password input becomes invalid');

        this.typeMessage($passwordInput, 'This is a message longer than twenty characters');
        strictEqual(this.iconsOnPage(), 0, 'No icon is shown when a password input becomes valid');

        var $textareaInput = this.addInputToElement({
            'data-aui-validate-minlength': '20'
        }, AJS.$('#qunit-fixture'), '<textarea>');
        this.typeMessage($textareaInput, 'Too short');
        strictEqual(this.iconsOnPage(), 1, 'An icon is shown when a textarea becomes invalid');

        this.typeMessage($textareaInput, 'This is a message longer than twenty characters');
        strictEqual(this.iconsOnPage(), 0, 'No icon is shown when a textarea becomes valid');
    });

    test('Invalidation is added to fields that are divs', function() {
        var $input = this.addInputToElement({
            'data-aui-validate-alwaysinvalidate': '20'
        }, AJS.$('#qunit-fixture'), '<div>');

        AJS.validator.validate($input);
        ok(this.fieldIsInvalid($input), 0, 'Non input fields still get invalid class');

    });

    test('Non text input fields tooltips are unaffected by blur', function() {
        var $input = this.addInputToElement({
            'data-aui-validate-alwaysinvalidate': ''
        }, AJS.$('#qunit-fixture'), '<div>');

        AJS.validator.validate($input);

        $input.focus();
        $input.blur();

        ok(this.tipsyOnPage(), 'Tooltip is present on page after blur on <div>');
    });

    test('Text input field tooltips are hidden after blur', function() {
        var $input = this.addInputToElement({
            'data-aui-validate-alwaysinvalidate': ''
        }, AJS.$('#qunit-fixture'));

        AJS.validator.validate($input);

        $input.focus();
        ok(this.tipsyOnPage(), 'Tooltip shown before blur on <input>');
        $input.blur();
        ok(!this.tipsyOnPage(), 'Tooltip is hidden after blur on <input type="text">');

    });

    test('Plugged in validators can be used by fields', function() {
        AJS.validator.register(['testvalidator'], function(field){
            if (field.$el.val().length !== 3) {
                field.invalidate('not length three');
            } else {
                field.validate();
            }
        });

        var $input = this.addInputToElement({
            'data-aui-validate-testvalidator': ''
        }, AJS.$('#qunit-fixture'));

        this.typeMessage($input, '1234');
        ok(this.fieldIsInvalid($input), 'Field is invalid when custom validator\'s conditions are not met');
        this.typeMessage($input, '123');
        ok(this.fieldIsValid($input), 'Field is valid when custom validator\'s conditions are met');

    });

    test('Initialised fields have correct field validation class', function() {
        var $input = this.addInputToElement({'data-aui-validate-maxlength': '1'}, AJS.$('#qunit-fixture'));
        ok($input.hasClass('aui-validation-field'), 'Initialised field starts with aui validation class');
    });

    test('Manual revalidation works', function(){
        var $input = this.addInputToElement({
            'data-aui-validate-minlength': '20'
        }, AJS.$('#qunit-fixture'));
        $input.val('too short');
        AJS.validator.validate($input);
        ok(this.fieldIsInvalid($input), 'Field becomes invalid after manual validation');
    });

    test('Whole form validation triggers validsubmit', function() {
        expect(1);

        var $form = $('<form></form>');
        AJS.$('#qunit-fixture').append($form);

        var $input = this.addInputToElement({
            'data-aui-validate-minlength': '1'
        }, $form);

        this.typeMessage($input, 'this message is long enough');

        $form.one('aui-valid-submit', function(e) {
            e.preventDefault();
            ok(true, 'Valid submit is triggered when the form is valid');
        });

        $form.trigger('submit');
    });

    test('Cannot register plugins with reserved arguments', function() {
        var registeredValidator = AJS.validator.register(['watch'], function(){});
        ok(!registeredValidator, 'Validator registration failed when a plugin with a reserved argument was registered');
    });

    test('Custom error messages display correctly', function() {
        var maxValue = 20;
        var customMessage = 'Custom message, needs to be less than ';
        var customMessageUnformatted = customMessage + '{0}';
        var customMessageFormatted = customMessage + maxValue;

        var $input = this.addInputToElement({
            'data-aui-validate-max': maxValue,
            'data-aui-validate-max-msg': customMessageUnformatted
        }, AJS.$('#qunit-fixture'));


        this.typeMessage($input, 'Invalid number');
        ok(this.fieldIsInvalid($input), 'Field becomes invalid after typing an invalid number with a custom message');

        this.typeMessage($input, '21');
        strictEqual(this.firstTipsyOnPageMessage(), customMessageFormatted);
    });

});